﻿ using ADM_Inperio_Paulista.AcessoDados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ADM_Inperio_Paulista.AcessoDados.CourseDatabase;

namespace ADM_Imperio_Paulista_Beta
{
    public class Login
    {

        public bool login(string usuario, string senha)
        {
            if (usuario == string.Empty || senha == string.Empty)
            {
                throw new Exception("Informe as credenciais.");
            }

            LoginDatabase loginDb = new LoginDatabase();
            bool isLogged = loginDb.Logar(usuario, senha);

            return isLogged;
        }
    }
}
