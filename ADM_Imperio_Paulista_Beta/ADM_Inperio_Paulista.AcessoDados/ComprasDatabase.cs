﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADM_Inperio_Paulista.AcessoDados
{
    public class ComprasDatabase
    {
        public void SaveCompras (ComprasDTO compra)
        {
            var query = "insert into tb_Compra (nr_arroz,nr_feijao,nr_banana,nr_ovo,nr_linguiça,nr_bisteca,nr_torresmo,nr_queijo,nr_verduras,nr_carne,nr_frutas) VALEUS {@nr_arroz,nr_feijao,nr_banana,nr_ovo,nr_linguiça,nr_bisteca,nr_torresmo,@nr_queijo,@nr_verduras,@nr_carne,@nr_frutas})";

            string script = string.Format(query, compra.Arroz, compra.Feijao, compra.Banana, compra.Ovo, compra.Linguica, compra.Bisteca, compra.Torresmo, compra.Queijo, compra.Verduras, compra.Carne, compra.Frutas);

            List<MySqlParameter> parameters = new List<MySqlParameter>();
            parameters.Add(new MySqlParameter("nr_arroz ", compra.Arroz));
            parameters.Add(new MySqlParameter("nr_feijao ", compra.Feijao));
            parameters.Add(new MySqlParameter("nr_ovo ", compra.Ovo));
            parameters.Add(new MySqlParameter("nr_linguiça ", compra.Linguica));
            parameters.Add(new MySqlParameter("nr_bisteca ", compra.Bisteca));
            parameters.Add(new MySqlParameter("nr_torresmo ", compra.Torresmo));
            parameters.Add(new MySqlParameter("nr_queijo ", compra.Queijo));
            parameters.Add(new MySqlParameter("nr_verduras ", compra.Verduras));
            parameters.Add(new MySqlParameter("nr_carne ", compra.Carne));
            parameters.Add(new MySqlParameter("nr_frutas ", compra.Frutas));


            Database bf = new Database();
            bf.ExecuteInsertScript(script, parameters);


        }
    }
}
