﻿using ADM_Inperio_Paulista_Beta.Modelos;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADM_Inperio_Paulista.AcessoDados
{
    public class PratosDatabase
    {
        public void SavePratos (PratosDTO pratos)
        {
            var query = "INSERT INTO tb_Pratos(nr_Virada,nr_Baiao,nr_Feijoada,nr_Escondidinho,nr_Peixes) VALEUS {@nr_Virada,@nr_Baiao,@nr_Feijoada,@nr_Escondidinho,@nr_Peixes})";

            string stript = string.Format (query, pratos.Virada , pratos.Baiao , pratos.Feijoada, pratos.Escondidinho, pratos.Peixes);

            List<MySqlParameter> parameters = new List<MySqlParameter>();
            parameters.Add(new MySqlParameter("nr_Virada ",pratos. Virada));
            parameters.Add(new MySqlParameter("nr_Baiao ", pratos.Baiao));
            parameters.Add(new MySqlParameter("nr_Feijoada ", pratos.Feijoada));
            parameters.Add(new MySqlParameter("nr_Escondidinho ", pratos.Escondidinho));
            parameters.Add(new MySqlParameter("nr_Peixes ", pratos.Peixes));



            Database bf = new Database();
            bf.ExecuteInsertScript(stript,parameters);
            
        }
        public  List<PratosDTO> ListAllCourse(string script)
        {
            var query = "SELEC * FROM tb_Pratos";
          

            Database db = new Database();
            MySqlDataReader reade = db.ExecuteSelectScript(query, null);
            List<PratosDTO> pratos = new List<PratosDTO>();

            while (reade.Read() == true)
            {
                PratosDTO prat = new PratosDTO();
                prat.Id = reade.GetInt32("id_pratos");
                prat.Virada = reade.GetInt32("nr_Virada");
                prat.Baiao = reade.GetInt32("nr_Baiao");
                prat.Feijoada = reade.GetInt32("nr_Feijoada");
                prat.Escondidinho = reade.GetInt32("nr_Escondidinho");
                prat.Peixes = reade.GetInt32("nr_Peixes");

                
            }

            return pratos;


        }
    }
}
