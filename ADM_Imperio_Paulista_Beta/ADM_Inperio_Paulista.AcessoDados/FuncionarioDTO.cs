﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADM_Inperio_Paulista.AcessoDados
{
    public class FuncionarioDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public int Telefone { get; set; }
        public int Cep { get; set; }
        public int Cnpj { get; set; }
        public string Email { get; set; }
        public int Senha { get; set; }
        public CourseDTO Course { get; set; }
    }
}
