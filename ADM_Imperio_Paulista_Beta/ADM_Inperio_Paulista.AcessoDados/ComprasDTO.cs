﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADM_Inperio_Paulista.AcessoDados
{
    public class ComprasDTO
    {
        public int Id { get; set; }
        public int Arroz { get; set; }
        public int Bisteca { get; set; }
        public int Feijao { get; set; }
        public int Banana { get; set; }
        public int Ovo { get; set; }
        public int Linguica { get; set; }
        public int Torresmo { get; set; }
        public int Queijo { get; set; }
        public int Verduras { get; set; }
        public int Carne { get; set; }
        public int Frutas { get; set; }

    }
}
