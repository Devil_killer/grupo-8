﻿using MySql.Data.MySqlClient;

namespace ADM_Inperio_Paulista.AcessoDados
{

    public class CourseDTO

    {
        public int Id { get; set; }
        public string Login { get; set; }
        public int Senha { get; set; }
        public CourseDTO Course { get; set; }
    }
}
