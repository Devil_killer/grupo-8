﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADM_Inperio_Paulista.AcessoDados
{
    public class LoginDatabase
    {
        public bool Logar(string usuario, string senha)
        {
            string script = "SELECT * FROM tb_Usuario WHERE ds_usuario = @usuario AND nr_senha = @senha";

            List<MySqlParameter> parameters = new List<MySqlParameter>();
            parameters.Add(new MySqlParameter("usuario", usuario));
            parameters.Add(new MySqlParameter("senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parameters);


            if (reader.Read() == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}