﻿namespace ADM_Imperio_Paulista_Beta
{
    partial class Compras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAdd = new System.Windows.Forms.Button();
            this.gvCompras = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Arroz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.feijao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Banana = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ovo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Linguiça = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Bisteca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Torresmo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Queijo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Verduras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Carne = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Frutas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvCompras)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtAdd);
            this.groupBox1.Controls.Add(this.gvCompras);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(822, 322);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Compras";
            // 
            // txtAdd
            // 
            this.txtAdd.Location = new System.Drawing.Point(724, 288);
            this.txtAdd.Name = "txtAdd";
            this.txtAdd.Size = new System.Drawing.Size(92, 28);
            this.txtAdd.TabIndex = 6;
            this.txtAdd.Text = "Ok";
            this.txtAdd.UseVisualStyleBackColor = true;
            this.txtAdd.Click += new System.EventHandler(this.button1_Click);
            // 
            // gvCompras
            // 
            this.gvCompras.AllowUserToAddRows = false;
            this.gvCompras.AllowUserToDeleteRows = false;
            this.gvCompras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvCompras.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Arroz,
            this.feijao,
            this.Banana,
            this.Ovo,
            this.Linguiça,
            this.Bisteca,
            this.Torresmo,
            this.Queijo,
            this.Verduras,
            this.Carne,
            this.Frutas});
            this.gvCompras.Location = new System.Drawing.Point(6, 105);
            this.gvCompras.Name = "gvCompras";
            this.gvCompras.ReadOnly = true;
            this.gvCompras.Size = new System.Drawing.Size(810, 157);
            this.gvCompras.TabIndex = 4;
            this.gvCompras.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick_1);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // Arroz
            // 
            this.Arroz.HeaderText = "Arroz";
            this.Arroz.Name = "Arroz";
            this.Arroz.ReadOnly = true;
            // 
            // feijao
            // 
            this.feijao.HeaderText = "Feijão";
            this.feijao.Name = "feijao";
            this.feijao.ReadOnly = true;
            // 
            // Banana
            // 
            this.Banana.HeaderText = "Banana ";
            this.Banana.Name = "Banana";
            this.Banana.ReadOnly = true;
            // 
            // Ovo
            // 
            this.Ovo.HeaderText = "Ovo";
            this.Ovo.Name = "Ovo";
            this.Ovo.ReadOnly = true;
            // 
            // Linguiça
            // 
            this.Linguiça.HeaderText = "Linguiça";
            this.Linguiça.Name = "Linguiça";
            this.Linguiça.ReadOnly = true;
            // 
            // Bisteca
            // 
            this.Bisteca.HeaderText = "Bisteca";
            this.Bisteca.Name = "Bisteca";
            this.Bisteca.ReadOnly = true;
            // 
            // Torresmo
            // 
            this.Torresmo.HeaderText = "Torresmo";
            this.Torresmo.Name = "Torresmo";
            this.Torresmo.ReadOnly = true;
            // 
            // Queijo
            // 
            this.Queijo.HeaderText = "Queijo";
            this.Queijo.Name = "Queijo";
            this.Queijo.ReadOnly = true;
            // 
            // Verduras
            // 
            this.Verduras.HeaderText = "Verduras";
            this.Verduras.Name = "Verduras";
            this.Verduras.ReadOnly = true;
            // 
            // Carne
            // 
            this.Carne.HeaderText = "Carne";
            this.Carne.Name = "Carne";
            this.Carne.ReadOnly = true;
            // 
            // Frutas
            // 
            this.Frutas.HeaderText = "Frutas";
            this.Frutas.Name = "Frutas";
            this.Frutas.ReadOnly = true;
            // 
            // Compras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.ClientSize = new System.Drawing.Size(842, 341);
            this.Controls.Add(this.groupBox1);
            this.Name = "Compras";
            this.Text = "Compras";
            this.Load += new System.EventHandler(this.Compras_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvCompras)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button txtAdd;
        private System.Windows.Forms.DataGridView gvCompras;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Arroz;
        private System.Windows.Forms.DataGridViewTextBoxColumn feijao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Banana;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ovo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Linguiça;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bisteca;
        private System.Windows.Forms.DataGridViewTextBoxColumn Torresmo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Queijo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Verduras;
        private System.Windows.Forms.DataGridViewTextBoxColumn Carne;
        private System.Windows.Forms.DataGridViewTextBoxColumn Frutas;
    }
}