﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ADM_Imperio_Paulista_Beta
{
    public partial class frmSplash : Form
    {
        public frmSplash()
        {
            InitializeComponent();
        }

        private void frmSplash_Load(object sender, EventArgs e)
        {

            // Configurações para se tornar Splash Screen
            BackColor = Color.Black;
            TransparencyKey = Color.Black;
            FormBorderStyle = FormBorderStyle.None;
            StartPosition = FormStartPosition.CenterScreen;
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            // Coloca a Imagem que será a Splash Screen
            BackgroundImage = Properties.Resources.imperio;


            // Inicia contagem para término da Splash Screen
            Task.Factory.StartNew(() =>
            {
                // Espera 2 segundos para iniciar o sistema
                System.Threading.Thread.Sleep(2000);

                Invoke(new Action(() =>
                {
                    // Abre a tela Inicial
                    Form2 frm = new Form2();
                    frm.Show();
                    Hide();
                }));
            });
        }
    }
}
