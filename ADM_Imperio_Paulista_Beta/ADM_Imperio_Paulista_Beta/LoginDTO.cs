﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADM_Imperio_Paulista_Beta.Dto
{
    public class LoginDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Period { get; set; }
        public string Class { get; set; }
        public int Year { get; set; }
    }

}
