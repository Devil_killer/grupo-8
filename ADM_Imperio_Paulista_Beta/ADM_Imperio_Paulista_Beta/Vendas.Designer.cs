﻿namespace ADM_Imperio_Paulista_Beta
{
    partial class Vendas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Virada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Baiao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Feijoada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Escondidinho = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Peixes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(667, 312);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vendas";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Virada,
            this.Baiao,
            this.Feijoada,
            this.Escondidinho,
            this.Peixes});
            this.dataGridView1.Location = new System.Drawing.Point(6, 99);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(655, 200);
            this.dataGridView1.TabIndex = 3;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(339, 64);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(99, 27);
            this.btnBuscar.TabIndex = 1;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(444, 67);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(193, 24);
            this.textBox1.TabIndex = 2;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            // 
            // Virada
            // 
            this.Virada.DataPropertyName = "Virada";
            this.Virada.HeaderText = "Virada";
            this.Virada.Name = "Virada";
            this.Virada.ReadOnly = true;
            // 
            // Baiao
            // 
            this.Baiao.DataPropertyName = "Baiao";
            this.Baiao.HeaderText = "Baiao";
            this.Baiao.Name = "Baiao";
            this.Baiao.ReadOnly = true;
            // 
            // Feijoada
            // 
            this.Feijoada.DataPropertyName = "Feijoada";
            this.Feijoada.HeaderText = "Feijoada";
            this.Feijoada.Name = "Feijoada";
            this.Feijoada.ReadOnly = true;
            // 
            // Escondidinho
            // 
            this.Escondidinho.DataPropertyName = "Escondidinho";
            this.Escondidinho.HeaderText = "Escondidinho";
            this.Escondidinho.Name = "Escondidinho";
            this.Escondidinho.ReadOnly = true;
            // 
            // Peixes
            // 
            this.Peixes.DataPropertyName = "Peixes";
            this.Peixes.HeaderText = "Peixes";
            this.Peixes.Name = "Peixes";
            this.Peixes.ReadOnly = true;
            // 
            // Vendas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.ClientSize = new System.Drawing.Size(692, 319);
            this.Controls.Add(this.groupBox1);
            this.Name = "Vendas";
            this.Text = "Vendas";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Virada;
        private System.Windows.Forms.DataGridViewTextBoxColumn Baiao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Feijoada;
        private System.Windows.Forms.DataGridViewTextBoxColumn Escondidinho;
        private System.Windows.Forms.DataGridViewTextBoxColumn Peixes;
    }
}