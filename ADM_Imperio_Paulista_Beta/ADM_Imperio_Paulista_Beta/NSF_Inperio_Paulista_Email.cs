﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;

namespace ADM_Imperio_Paulista_Beta
{
    public partial class NSF_Inperio_Paulista_Email : Form
    {
        public NSF_Inperio_Paulista_Email()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            
        }

        void EnviarComGmail(string para, string copia, string copiaOculta, string assunto, string mensagem, bool comHtml)
        {
            string origem = "emailRemetente@GMAIL.COM";
            string senha = "s3nh@";
            MailMessage email = new MailMessage();
            MailAddress emailPara = new MailAddress(para);
            MailAddress emailCopia = new MailAddress(copia);
            MailAddress emailCopiaOculta = new MailAddress(copiaOculta);
            email.From = new MailAddress(origem);
            email.To.Add(emailPara);
            email.CC.Add(emailCopia);
            email.Bcc.Add(emailCopiaOculta);
            email.Subject = assunto;
            email.Body = mensagem;
            email.IsBodyHtml = comHtml;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(origem, senha);
            smtp.Send(email);
        }

    }
}
