﻿using ADM_Inperio_Paulista.AcessoDados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADM_Inperio_Paulista_Beta.Modelos
{
    public class PratosDTO
    {
        public int Id { get; set; }
        public int Virada { get; set; }
        public int Baiao { get; set; }
        public int Feijoada { get; set; }
        public int Escondidinho { get; set; }
        public int Peixes { get; set; }

        public ComprasDTO Compras { get; set; }
     }
}
