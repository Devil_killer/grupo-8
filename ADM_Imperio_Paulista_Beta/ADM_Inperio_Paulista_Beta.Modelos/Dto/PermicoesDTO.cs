﻿using ADM_Imperio_Paulista_Beta.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADM_Inperio_Paulista_Beta.Modelos.Dto
{
    public class PermicoesDTO
    {
        public int Id { get; set; }
        public Boolean Rh { get; set; }
        public Boolean Financeiro { get; set; }
        public Boolean Compras { get; set; }
        public Boolean Vendas { get; set; }

        public CadasdroDTO Permicoes { get; set; }

    }
}
